package com.russo.gentree;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.russo.gentree.model.DataSource;
import com.russo.gentree.model.Person;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class SelectParentActivity extends Activity implements OnItemClickListener, OnClickListener
{
	Button backButton;
	
	ArrayList<Map<String, Object>> data;
	SimpleAdapter sAdapter;
	ListView lvSimple;
	
	final String ATTRIBUTE_NAME_TEXT = "text";
	final String ATTRIBUTE_NAME_IMAGE = "image";
	final String ATTRIBUTE_PERSON = "person";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		  super.onCreate(savedInstanceState);
		  setContentView(R.layout.select_parent);
		  
		  backButton = (Button)findViewById(R.id.back_to_edit_person_id);
		  backButton.setOnClickListener(this);
		  
		  data = new ArrayList<Map<String, Object>>();
		  readData();
		  String[] from = { ATTRIBUTE_NAME_TEXT, ATTRIBUTE_NAME_IMAGE };
	      int[] to = { R.id.tvText, R.id.ivImg };

	      sAdapter = new SimpleAdapter(this, data, R.layout.person_item, from, to);
	        
		  lvSimple = (ListView) findViewById(R.id.listView1);
		  lvSimple.setAdapter(sAdapter);
		  lvSimple.setOnItemClickListener(this);
		     
		  sAdapter.notifyDataSetChanged();
	}
	
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		Intent intent = getIntent();
		int pid = intent.getIntExtra("person_id", -1);
		DataSource dataSource = DataSource.sharedDataSource(null);
		Person updatedPerson = dataSource.getPersonWithId(pid);
		Person selectedPerson = getPersonWithIndex(position);
		
		int sex = intent.getIntExtra("sex_person", -1);
		if (sex == Person.MALE)
		{
			updatedPerson.setFather(selectedPerson);
		}
		else if (sex == Person.FEMALE)
		{
			updatedPerson.setMather(selectedPerson);
		}
		dataSource.updatePerson(updatedPerson);
		backToPreviousActivity();
	}
	
	private Person getPersonWithIndex(int index)
	{
		 Map<String, Object> m = data.get(index);
		 return m != null ? (Person) m.get(ATTRIBUTE_PERSON) : null;
	}
	
	@Override
	public void onClick(View v)
	{
		if (v.getId() == R.id.back_to_edit_person_id)
		{
			backToPreviousActivity();
		}
	}
	
	private void backToPreviousActivity()
	{
		finish();
	}
	
	private void readData()
	{
		Intent intent = getIntent();
		int sex = intent.getIntExtra("sex_person", -1);
		int pid = intent.getIntExtra("person_id", -1);
		
		ArrayList<Person> persons = getAllPersonsOnly(sex, pid);
		int img = R.drawable.ic_launcher;
		for (Person person : persons)
		{
			Map<String, Object> m = new HashMap<String, Object>();
         	m.put(ATTRIBUTE_NAME_TEXT, person.getFIO());
         	m.put(ATTRIBUTE_NAME_IMAGE, img);
         	m.put(ATTRIBUTE_PERSON, person);
         	data.add(m);
		}
	}
	
	private ArrayList<Person> getAllPersonsOnly(int sex, int exclusionId)
	{
		ArrayList<Person> persons = new ArrayList<Person>();
		DataSource dataSource = DataSource.sharedDataSource(null);
		ArrayList<Person> allPersons = dataSource.getAllPersons();
		for (Person person : allPersons)
		{
			if (person.getSex() == sex && person.getPId() != exclusionId)
			{
				persons.add(person);
			}
		}
		return persons;
	}
}
