package com.russo.gentree.model;

import java.util.ArrayList;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataSource
{
	private static DataSource instance = null;
	private ArrayList<Person> persons;
	DBHelper dbHelper;
	
	public final String _LOG_TAG = "myLogs";
	
	public static DataSource sharedDataSource(Context context)
	{
		if (instance == null)
		{
			instance = new DataSource(context);
		}
		return instance;
	}
	
	public DataSource(Context context)
	{
		dbHelper = new DBHelper(context);
	}
	
	public ArrayList<Person> getAllPersons()
	{
		if (persons == null)
		{
			persons = new ArrayList<Person>();
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			Cursor c = db.query("persons_table", null, null, null, null, null, null);
			if (c.moveToFirst())
		    {
		        int idColIndex = c.getColumnIndex("id");
		        int nameColIndex = c.getColumnIndex("name");
		        int lastName = c.getColumnIndex("last_name");
		        int fatherName = c.getColumnIndex("father_name");
		        int sexIndex = c.getColumnIndex("sex");
		        int birthPlaceIndex = c.getColumnIndex("birth_place");
		        int matherIdIndex = c.getColumnIndex("mather_id");
		        int fatherIdIndex = c.getColumnIndex("father_id");
		        
		        do
		        {
	             	Person person = new Person(c.getInt(idColIndex));
	             	person.setName(c.getString(nameColIndex));
	             	person.setLastName(c.getString(lastName));
	             	person.setFatherName(c.getString(fatherName));
	             	person.setSex(c.getInt(sexIndex));
	             	person.setBirthPlace(c.getString(birthPlaceIndex));
	             	person.setMatherId(c.getInt(matherIdIndex));
	             	person.setFatherId(c.getInt(fatherIdIndex));
	             	persons.add(person);
	             	
	             	Log.d(_LOG_TAG,
		              "ID = " + c.getInt(idColIndex) + 
		              ", name = " + c.getString(nameColIndex) + 
		              ", last_name = " + c.getString(lastName) +
		              ", father_name = " + c.getString(fatherName));
		        } while (c.moveToNext());
		        initializeParents();
		    }
		    else
		    {
		    	Log.d(_LOG_TAG, "0 rows");
		    }
		    dbHelper.close();
		}
		return persons;
	}
	
	private void initializeParents()
	{
		for (Person person : persons)
		{
			Person mather = getPersonWithId(person.getMatherId());
			if (mather != null)
			{
				person.setMather(mather);
			}
			Person father = getPersonWithId(person.getFatherId());
			if (father != null)
			{
				person.setFather(father);
			}
		}
	}
	
	public int removeAllPersons()
	{
		Log.d(_LOG_TAG, "--- Clear mytable: ---");
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int clearCount = db.delete("persons_table", null, null);
		if (clearCount == persons.size())
		{
			while (persons.size() > 0)
			{
				persons.remove(persons.size() - 1);
			}
		}
		dbHelper.close();
		return clearCount;
	}
	
	private int removePersonWithId(int pid)
	{
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		String strId = "id = " + Integer.toString(pid);
		int delCount = db.delete("persons_table", strId, null);
		dbHelper.close();
		return delCount;
	}
	
	public int removePerson(Person removedPerson)
	{
		int removedCount = removePersonWithId(removedPerson.getPId());
		if (removedCount == 1)
		{
			persons.remove(removedPerson);
		}
		return removedCount;
	}
	
	public long addPerson(Person addedPerson)
	{
		SQLiteDatabase db = dbHelper.getWritableDatabase();
	    ContentValues cv = new ContentValues();
	    
	    Log.d(_LOG_TAG, "--- Insert in persons_table: ---");
	    
	    cv.put("name", addedPerson.getName());
	    cv.put("last_name", addedPerson.getLastName());
	    cv.put("father_name", addedPerson.getFatherName());
	    cv.put("birth_place", addedPerson.getBirthPlace());
	    cv.put("sex", addedPerson.getSex());
	    cv.put("mather_id", addedPerson.getMatherId());
	    cv.put("father_id", addedPerson.getFatherId());
	    
	    long rowID = db.insert("persons_table", null, cv);
	    addedPerson.setPID((int)rowID);
	    persons.add(addedPerson);
	    Log.d(_LOG_TAG, "row inserted, ID = " + rowID);
		dbHelper.close();
		return rowID;
	}
	
	public int updatePerson(Person updatedPerson)
	{
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues cv = new ContentValues();
		
		Log.d(_LOG_TAG, "--- Update persons_table: ---");
	    
	    cv.put("name", updatedPerson.getName());
	    cv.put("last_name", updatedPerson.getLastName());
	    cv.put("father_name", updatedPerson.getFatherName());
	    cv.put("birth_place", updatedPerson.getBirthPlace());
	    cv.put("sex", updatedPerson.getSex());
	    cv.put("mather_id", updatedPerson.getMatherId());
	    cv.put("father_id", updatedPerson.getFatherId());
	    
	    String pid = Integer.toString(updatedPerson.getPId());
		int updCount = db.update("persons_table", cv, "id = ?", new String[] { pid });
		Log.d(_LOG_TAG, "updated rows count = " + updCount);
		dbHelper.close();
		return updCount;
	}
	
	public Person getPersonWithId(int pid)
	{
		for (Person person : persons)
		{
			if (person.getPId() == pid)
				return person;
		}
		return null;
	}
}

class DBHelper extends SQLiteOpenHelper
{
    private static final String LOG_TAG = "myLogs";

	public DBHelper(Context context)
    {
    	super(context, "myDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
      Log.d(LOG_TAG, "--- onCreate database ---");
      
      db.execSQL("create table persons_table ("
          + "id integer primary key autoincrement," 
          + "name text,"
          + "last_name text,"
          + "father_name text,"
          + "sex integer,"
          + "birth_place text,"
          + "mather_id integer,"
          + "father_id integer" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }
 }
