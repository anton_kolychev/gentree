package com.russo.gentree.view;


import com.russo.gentree.model.DataSource;
import com.russo.gentree.model.Person;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


public class TreeActivity extends Activity
{

	  @Override
	  protected void onCreate(Bundle savedInstanceState)
	  {
		  super.onCreate(savedInstanceState);
		  
		  Intent intent = getIntent();
		  int pid = intent.getIntExtra("person_id", -1);
		  DataSource dataSource = DataSource.sharedDataSource(null);
		  Person currentPerson = dataSource.getPersonWithId(pid);
		  setContentView(new TreeView(this, currentPerson));
	  }
}
