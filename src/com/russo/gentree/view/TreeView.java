package com.russo.gentree.view;


import com.russo.gentree.model.Person;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

public class TreeView extends View
{
	    
	    Paint p;
	    Rect rect;
	    private Person currentPerson;

	    public TreeView(Context context, Person person)
	    {
	      super(context);
	      p = new Paint();
	      rect = new Rect();
	      this.currentPerson = person;
	    }
	    
	    @Override
	    protected void onDraw(Canvas canvas)
	    {
	    	canvas.drawARGB(80, 102, 204, 255);
	    	currentPerson.setLevel(0);
	    	currentPerson.setTreeIndex(0);
	    	drawPersonOnCanvas(currentPerson, canvas);
	    }
	    
	    private void drawPersonOnCanvas(Person person, Canvas canvas)
	    {
	    	person.drawOnCanvas(canvas);
	    	Person mather = person.getMather();
	    	if (mather != null)
	    	{
	    		mather.setLevel(person.getLevel() + 1);
	    		mather.setTreeIndex(0);
	    		drawPersonOnCanvas(mather, canvas);
	    	}
	    	Person father = person.getFather();
	    	if (father != null)
	    	{
	    		father.setLevel(person.getLevel() + 1);
	    		father.setTreeIndex(1);
	    		drawPersonOnCanvas(father, canvas);
	    	}
	    }
}
