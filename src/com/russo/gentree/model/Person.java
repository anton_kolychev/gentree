package com.russo.gentree.model;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Person
{
	private int pId;
	private String name;
	private String lastName;
	private String fatherName;
	private int sex;
	private String birthPlace;
	private Person mather;
	private Person father;
	private int matherId;
	private int fatherId;
	private ArrayList<Person> children;
	private int level = -1;
	private int treeIndex = -1;
	private float x;
	private float y;
	
	public static final int MALE = 0;
	public static final int FEMALE = 1;
	
	public static final int WIDTH = 200;
	public static final int HEIGHT = 150;
	
	public Person(int pid)
	{
		this.children = new ArrayList<Person>();
		pId = pid;
	}
	
	public Person()
	{
		this.children = new ArrayList<Person>();
	}
	
	public String getFatherName()
	{
		return fatherName;
	}
	
	public void setFatherName(String fatherName)
	{
		this.fatherName = fatherName;
	}

	public int getSex()
	{
		return sex;
	}

	public void setSex(int sex)
	{
		this.sex = sex;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	} 
	
	public int getPId()
	{
		return pId;
	}
	
	public void setPID(int pid)
	{
		this.pId = pid;
	}
	
	public String getFIO()
	{
		return this.lastName + " " + this.name + " " + this.fatherName;
	}
	
	public void saveInDatabase()
	{
		
	}

	public String getBirthPlace()
	{
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace)
	{
		this.birthPlace = birthPlace;
	}

	public Person getMather()
	{
		return mather;
	}

	public void setMather(Person mather)
	{
		this.mather = mather;
		this.matherId = mather.getPId();
		mather.getChildren().add(this);
	}

	public Person getFather()
	{
		return father;
	}

	public void setFather(Person father)
	{
		this.father = father;
		this.fatherId = father.getPId();
		father.getChildren().add(this);
	}
	
	public int getMatherId()
	{
		return matherId;
	}
	
	public void setMatherId(int matherId)
	{
		this.matherId = matherId;
	}
	
	public int getFatherId()
	{
		return fatherId;
	}
	
	public void setFatherId(int fatherId)
	{
		this.fatherId = fatherId;
	}
	
	public ArrayList<Person> getChildren()
	{
		return this.children;
	}
	
	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public int getTreeIndex()
	{
		return treeIndex;
	}

	public void setTreeIndex(int treeIndex)
	{
		this.treeIndex = treeIndex;
	}
	
	public float getX() 
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}
	
	public void drawOnCanvas(Canvas canvas)
	{
		this.x = this.treeIndex * WIDTH + 30 * this.treeIndex + 20;
		this.y = this.level * HEIGHT + 50 * this.level + 20;
		
		Rect rect = new Rect((int)this.x,(int)this.y, WIDTH + (int)this.x, HEIGHT + (int)this.y);
		Paint p = new Paint();
		p.setStrokeWidth(3);
		if (this.getSex() == Person.FEMALE)
		{
			p.setColor(Color.RED);
		}
		else
		{
			p.setColor(Color.BLUE);
		}
		p.setStyle(Paint.Style.FILL);
		canvas.drawRect(rect, p);
		p.setColor(Color.MAGENTA);
		p.setStyle(Paint.Style.STROKE);
	    canvas.drawRect(rect, p);
		
	    p.setStrokeWidth(2);
	    p.setColor(Color.BLACK);
		p.setTextSize(20);
		p.setTextAlign(Paint.Align.CENTER);
		
		int startPos = (int)this.x + WIDTH / 2;
		canvas.drawText(this.lastName, startPos, this.y + 22, p);
		canvas.drawText(this.name, startPos, this.y + 47, p);
		canvas.drawText(this.fatherName, startPos, this.y + 72, p);
		
		if (this.birthPlace != null && this.birthPlace.length() > 0)
		{
			canvas.drawText("<" + this.birthPlace + ">", startPos, this.y + 102, p);
		}
		
		for (Person child : getChildren())
		{
			float childX = child.getX();
			float childY = child.getY();
			
			float x1 = childX + WIDTH / 2;
			float y1 = childY + HEIGHT;
			
			float x2 = x1;
			float y2 = y1 + 25;
			
			float x3 = this.x + WIDTH / 2;
			float y3 = this.y - 25;
			
			float x4 = x3;
			float y4 = this.y;
			
			float points[] = { x1, y1, x2, y2, x2, y2, x3, y3, x3, y3, x4, y4};
			canvas.drawLines(points, p);
		}
	}

	
}
