package com.russo.gentree;

import com.russo.gentree.model.DataSource;
import com.russo.gentree.model.Person;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

public class AddPersonActivity extends Activity implements OnClickListener
{
	private static final String LOG_TAG = "myLogs";
	Button backButton;
	Button actionButton;
	Button setMatherButton;
	Button setFatherButton;
	Person currentPerson;
	
	EditText name;
	EditText lastName;
	EditText fatherName;
	EditText birthDate;
	EditText birthPlace;
	EditText matherFIOField;
	EditText fatherFIOField;
	
	RadioGroup sexRadioGroup;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_person);
		
		backButton = (Button)findViewById(R.id.back_button);
		backButton.setOnClickListener(this);
		
		setMatherButton = (Button)findViewById(R.id.add_mather_button_id);
		setMatherButton.setOnClickListener(this);
		
		setFatherButton = (Button)findViewById(R.id.add_father_button_id);
		setFatherButton.setOnClickListener(this);
		
		actionButton = (Button)findViewById(R.id.add_person_button);
		
		Intent intent = getIntent();
		String actionString = intent.getAction();
		OnClickListener actionButtonListener = null;
	    if (actionString.equals("com.russo.gentree.intent.action.add_person"))
	    {
	    	actionButton.setText("��������");
	    	actionButtonListener = new OnClickListener()
	    	{
	 	       @Override
	 	       public void onClick(View v) 
	 	       {
	 	    	  if (addPersonToBase())
	 	    	  {
	 	    		 goToPreviousActivity();
	 	    	  }
	 	       }
	 	    };
	    }
	    else if (actionString.equals("com.russo.gentree.intent.action.edit_person"))
	    {
	    	actionButton.setText("��������");
	    	actionButtonListener = new OnClickListener()
	    	{
	 	       @Override
	 	       public void onClick(View v) 
	 	       {
	 	    	  if (savePerson())
	 	    	  {
	 	    		  goToPreviousActivity();
	 	    	  }
	 	       }
	 	    };
	    }

	    actionButton.setOnClickListener(actionButtonListener);
	    
	    name = (EditText)findViewById(R.id.person_name_field);
	    lastName = (EditText)findViewById(R.id.person_last_name_field);
	    fatherName = (EditText)findViewById(R.id.person_father_name_field);
	    birthDate = (EditText)findViewById(R.id.birth_date_field);
	    birthPlace = (EditText)findViewById(R.id.birth_place_field);
	    sexRadioGroup = (RadioGroup)findViewById(R.id.sex_control);
	    matherFIOField = (EditText)findViewById(R.id.mather_name_field);
		fatherFIOField = (EditText)findViewById(R.id.father_name_field);
	    
	    OnCheckedChangeListener checkListener = new OnCheckedChangeListener() 
	    {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId)
			{
				setBackColorByCheckedId(checkedId);
			}
		};
	   sexRadioGroup.setOnCheckedChangeListener(checkListener);
	}
	
	private void updateFields()
	{
		Intent intent = getIntent();
		int editedPerson = intent.getIntExtra("edit_person", 0);
		DataSource dataSource = DataSource.sharedDataSource(null);
		currentPerson = dataSource.getPersonWithId(editedPerson);
		if (currentPerson != null)
		{
			name.setText(currentPerson.getName());
			lastName.setText(currentPerson.getLastName());
			fatherName.setText(currentPerson.getFatherName());
			birthPlace.setText(currentPerson.getBirthPlace());
			int checkedButtonId = currentPerson.getSex() == Person.MALE ? R.id.sex_male : R.id.sex_female;
			sexRadioGroup.check(checkedButtonId);
			setBackColorByCheckedId(checkedButtonId);
			Person mather = currentPerson.getMather();
			if (mather != null)
			{
				matherFIOField.setText(mather.getFIO());	
			}
			Person father = currentPerson.getFather();
			if (father != null)
			{
				fatherFIOField.setText(father.getFIO());
			}
		}
		else
		{
			setMatherButton.setEnabled(false);
			setFatherButton.setEnabled(false);
		}
		
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		Log.d(LOG_TAG, "AddPerson: onStart()");
		updateFields();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d(LOG_TAG, "AddPerson: onResume()");
	}
	
	private void setBackColorByCheckedId(int checkedId)
	{
		int backColor = checkedId == R.id.sex_male ? Color.BLUE : Color.RED;
		sexRadioGroup.setBackgroundColor(backColor);
	}
	
	@Override
	public void onClick(View v)
	{
		if (v.getId() == R.id.back_button)
		{
			this.goToPreviousActivity();
		}
		else if (v.getId() == R.id.add_mather_button_id)
		{
			Intent intent = new Intent(this, SelectParentActivity.class);
			intent.putExtra("sex_person", Person.FEMALE);
			if (currentPerson != null)
			{
				intent.putExtra("person_id", currentPerson.getPId());
			}
			startActivity(intent);
		}
		else if (v.getId() == R.id.add_father_button_id)
		{
			Intent intent = new Intent(this, SelectParentActivity.class);
			intent.putExtra("sex_person", Person.MALE);
			if (currentPerson != null)
			{
				intent.putExtra("person_id", currentPerson.getPId());
			}
			startActivity(intent);
		}
	}
	
	private void goToPreviousActivity()
	{

		finish();
	}
	
	private boolean checkFields(String nameString, String lastNameString, String fatherNameString)
	{
		if (nameString.length() == 0 || 
			lastNameString.length() == 0 ||
			fatherNameString.length() == 0)
		{
			Toast.makeText(this, "������� ������ ���������", Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}
	
	private boolean updatePerson(Person person)
	{
		String nameString = name.getText().toString();
		String lastNameString = lastName.getText().toString();
		String fatherNameString = fatherName.getText().toString();
		if (!checkFields(nameString, lastNameString, fatherNameString))
			return false;
		
		person.setName(nameString);
		person.setLastName(lastNameString);
		person.setFatherName(fatherNameString);
		person.setBirthPlace(birthPlace.getText().toString());
		int chekedButtonId = sexRadioGroup.getCheckedRadioButtonId();
		person.setSex(chekedButtonId == R.id.sex_male ? Person.MALE : Person.FEMALE);
//		birthDate.get
		return true;
	}
	
	private boolean addPersonToBase()
	{
		Log.d(LOG_TAG, "add person");
		Person addedPerson = new Person();
		if(!updatePerson(addedPerson))
			return false;
		
		DataSource dataSource = DataSource.sharedDataSource(null);
		dataSource.addPerson(addedPerson);
		return true;
	}
	
	private boolean savePerson()
	{
		Log.d(LOG_TAG, "edit person");
		boolean result = false;
		if (currentPerson != null)
		{
			result = updatePerson(currentPerson);
			if (result)
			{
				DataSource dataSource = DataSource.sharedDataSource(null);
				dataSource.updatePerson(currentPerson);
			}
		}
		else
		{
			Toast.makeText(this, "Person is null", Toast.LENGTH_LONG).show();
		}
		return result;
	}
}
