package com.russo.gentree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.russo.gentree.model.DataSource;
import com.russo.gentree.model.Person;
import com.russo.gentree.view.TreeActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class MainActivity extends Activity implements OnClickListener, OnItemClickListener
{
	Button addPersonButton;
	Button removeAllButton;
	
	ListView lvSimple;
	
	final String LOG_TAG = "myLogs";
	private static final int CM_DELETE_ID = 1;
	private static final int CM_DRAW_TREE = 2;
	
	// ����� ��������� ��� Map
	final String ATTRIBUTE_NAME_TEXT = "text";
	final String ATTRIBUTE_NAME_IMAGE = "image";
	final String ATTRIBUTE_PERSON = "person";
	
	
	ArrayList<Map<String, Object>> data;
	SimpleAdapter sAdapter;
	
	private DataSource dataSource;
	 
	@Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Log.d(LOG_TAG, "MainActivity: onCreate()");
        
        addPersonButton = (Button)findViewById(R.id.addPersonButtonId);
        addPersonButton.setOnClickListener(this);
       
        removeAllButton = (Button)findViewById(R.id.removeAllPersonsButtonId);
        removeAllButton.setOnClickListener(this);
        
        dataSource = DataSource.sharedDataSource(this);
        
        data = new ArrayList<Map<String, Object>>();
       
        
        String[] from = { ATTRIBUTE_NAME_TEXT, ATTRIBUTE_NAME_IMAGE };
        int[] to = { R.id.tvText, R.id.ivImg };

        sAdapter = new SimpleAdapter(this, data, R.layout.person_item,
            from, to);
        
        lvSimple = (ListView) findViewById(R.id.personsListView);
        lvSimple.setAdapter(sAdapter);
        lvSimple.setOnItemClickListener(this);
        registerForContextMenu(lvSimple);
    }
	
	public void onItemClick(AdapterView<?> parent, View view,
	          int position, long id)
	{
		Log.d(LOG_TAG, "itemClick: position = " + position + ", id = " + id);
		Intent intent = new Intent("com.russo.gentree.intent.action.edit_person");
		Person person = getPersonWithIndex(position);
		if (person != null)
		{
			intent.putExtra("edit_person", person.getPId());
			startActivity(intent);
		}
	}
	
	private void readFromDataBase()
	{
		ArrayList<Person> persons = dataSource.getAllPersons();
		int img = R.drawable.ic_launcher;
		if (data != null)
        {
        	 while (data.size() > 0)
		     {
				data.remove(data.size() - 1);
		     }
        	 Log.d(LOG_TAG, "--- remove row");
        }
		for (Person person : persons)
		{
			Map<String, Object> m = new HashMap<String, Object>();
         	m.put(ATTRIBUTE_NAME_TEXT, person.getFIO());
         	m.put(ATTRIBUTE_NAME_IMAGE, img);
         	m.put(ATTRIBUTE_PERSON, person);
         	data.add(m);
		}
		sAdapter.notifyDataSetChanged();
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		Log.d(LOG_TAG, "MainActivity: onStart()");
		readFromDataBase();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d(LOG_TAG, "MainActivity: onResume()");
	}
	
    @Override
	public void onClick(View v)
    {
		if (v.getId() == R.id.addPersonButtonId)
		{
			Intent intent = new Intent("com.russo.gentree.intent.action.add_person");
			startActivity(intent);
		}
		else if (v.getId() == R.id.removeAllPersonsButtonId)
		{
		     int clearCount = dataSource.removeAllPersons();
		     Log.d(LOG_TAG, "deleted rows count = " + clearCount);
		     readFromDataBase();
		}
	}
    
    private Person getPersonWithIndex(int index)
    {
    	Map<String, Object> m = data.get(index);
    	return m != null ? (Person) m.get(ATTRIBUTE_PERSON) : null;
    }
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
        ContextMenuInfo menuInfo)
    {
      super.onCreateContextMenu(menu, v, menuInfo);
      menu.add(0, CM_DELETE_ID, 0, "������� �������");
      menu.add(0, CM_DRAW_TREE, 1, "��������� ������");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
    	AdapterContextMenuInfo acmi = (AdapterContextMenuInfo) item.getMenuInfo();
    	Person selectedPerson = this.getPersonWithIndex(acmi.position);
    	if (item.getItemId() == CM_DELETE_ID)
    	{
    		Log.d(LOG_TAG, "--- Clear mytable: ---");
    		
    		if (selectedPerson != null)
    		{
        		int delCount = dataSource.removePerson(selectedPerson);
    		    Log.d(LOG_TAG, "deleted rows count = " + delCount);
        		data.remove(acmi.position);
    		}
    		
    		sAdapter.notifyDataSetChanged();
    		return true;
    	}
    	else if (item.getItemId() == CM_DRAW_TREE)
    	{
    		if (selectedPerson != null)
    		{
    			Intent intent = new Intent(this, TreeActivity.class);
        		intent.putExtra("person_id", selectedPerson.getPId());
        	    startActivity(intent);
    		}
    		return true;
    	}
    	return super.onContextItemSelected(item);
    }
    
    //not used
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_settings)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    } 
}
